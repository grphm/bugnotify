<?php

return [
    'package_name' => 'solutions_bugnotify',
    'package_title' => ['ru' => 'Регистратор ошибок', 'en' => 'Bug Notifier', 'es' => 'Bichos del notificador'],
    'package_icon' => 'zmdi zmdi-shield-security',
    'relations' => [],
    'package_description' => [
        'ru' => 'Регистрирует ошибки в CMS. Уведомляет по электронной почте.',
        'en' => 'Errors are logged in the CMS. Notify via e-mail.',
        'es' => 'Los errores se registran en el CMS. Notificará por correo electrónico.'
    ],
    'version' => [
        'ver' => 1.1,
        'date' => '19.10.2016'
    ]
];