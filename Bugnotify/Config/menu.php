<?php

return [
    'package' => 'solutions_bugnotify',
    'title' => ['ru' => 'Регистратор ошибок', 'en' => 'Bug Notifier', 'es' => 'Bichos del notificador'],
    'route' => 'solutions.bugnotify.index',
    'icon' => 'zmdi zmdi-shield-security',
    'menu_child' => []
];