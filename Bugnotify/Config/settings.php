<?php

return [
    'bugnotify' => [
        'title' => ['ru' => 'Регистратор ошибок', 'en' => 'Bug Notifier', 'es' => 'Bichos del notificador'],
        'options' => [
            ['group_title' => ['ru' => 'Основные', 'en' => 'Main', 'es' => 'Los principales']],
            'mail_subject' => [
                'title' => [
                    'ru' => 'Тема письма',
                    'en' => 'Letter subject',
                    'es' => 'Sujeto'
                ],
                'note' => ['ru' => '', 'en' => '', 'es' => ''],
                'type' => 'text',
                'value' => 'STALKER CMS'
            ],
            'emails' => [
                'title' => [
                    'ru' => 'Адреса получателей',
                    'en' => 'Recipient emails',
                    'es' => 'Mensajes de correo electrónico del destinatario'
                ],
                'note' => [
                    'ru' => 'Перечислить через запятую',
                    'en' => 'List separated by commas',
                    'es' => 'Lista separada por comas'
                ],
                'type' => 'text',
                'value' => config('mail.from.address')
            ],
            'send_notification' => [
                'title' => ['ru' => 'Отправлять уведомления', 'en' => 'Send notifications', 'es' => 'Enviar notificaciones'],
                'note' => [
                    'ru' => 'Позволяет «отключать» уведомления по электронной почте',
                    'en' => 'It allows you to "turn off" a notification by e-mail',
                    'es' => 'Que le permite "apagar" una notificación por e-mail'
                ],
                'type' => 'checkbox',
                'value' => FALSE
            ],
        ]
    ]
];