<?php
namespace STALKER_CMS\Solutions\Bugnotify\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Фасад контроллера Bugnotify
 * Class Dictionary
 * @package STALKER_CMS\Core\Dictionaries\Facades
 */
class Bugnotify extends Facade {

    protected static function getFacadeAccessor() {

        return 'BugnotifyController';
    }
}