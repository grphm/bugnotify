<?php
namespace STALKER_CMS\Solutions\Bugnotify\Http\Controllers;

use Exception;
use STALKER_CMS\Solutions\Bugnotify\Models\Bugnotify;
use STALKER_CMS\Vendor\Http\Controllers\Controller;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\Debug\ExceptionHandler as SymfonyExceptionHandler;

class BugnotifyController extends Controller {

    protected $model;
    private $allowed = [];
    private $except = [404, 405, 2005];
    private $statusCode = 0;

    public function __construct() {

        $this->model = new Bugnotify();
        $this->middleware('auth');
    }

    protected function index() {

        \PermissionsController::allowPermission('solutions_bugnotify', 'bugnotify');
        return view('solutions_bugnotify_views::index', ['errors' => $this->model->orderBy('created_at', 'DESC')->paginate(15)]);
    }

    protected function destroy($id) {

        \PermissionsController::allowPermission('solutions_bugnotify', 'bugnotify');
        \RequestController::isAJAX()->init();
        $this->model->remove($id);
        return \ResponseController::success(1203)->redirect(route('solutions.bugnotify.index'))->json();
    }

    public function viewLaravelLog() {

        \PermissionsController::allowPermission('solutions_bugnotify', 'bugnotify');
        return view('solutions_bugnotify_views::view_log');
    }

    public function viewLaravelLogDownload() {

        if(\File::exists(storage_path('logs/laravel.log'))):
            return \Response::download(storage_path('logs/laravel.log'), 'laravel.log', ['Content-type' => 'text/plain']);
        endif;
        return redirect()->back();
    }

    /**
     * Установка кода статуса ошибки
     * @param $code
     * @return $this
     */
    public function setStatusCode($code) {

        $this->statusCode = $code;
        return $this;
    }

    /**
     * Добавление разрешенного кода
     * @param $code
     * @return $this
     */
    public function addAllowedCode($code) {

        if(!in_array($code, $this->allowed)):
            $this->allowed[] = $code;
        endif;
        return $this;
    }

    /**
     * Добавление запрещеного кода
     * @param $code
     * @return $this
     */
    public function addExceptCode($code) {

        if(!in_array($code, $this->except)):
            $this->except[] = $code;
        endif;
        return $this;
    }

    /**
     * Получить список запрещеных кодов
     * @return array
     */
    public function getExceptCodes() {

        return $this->except;
    }

    /**
     * Получить список разрешенных кодов
     * @return array
     */
    public function getAllowedCodes() {

        return $this->allowed;
    }

    /**
     * Уведомляе по электронной почте об критической ошибке
     * @param Exception $e
     * @return bool|$this
     */
    public function Notifier(Exception $e) {

        if(settings(['solutions_bugnotify', 'bugnotify', 'send_notification'])):
            if(!in_array($this->statusCode, $this->except)):
                if($emails = explode(',', settings(['solutions_bugnotify', 'bugnotify', 'emails']))):
                    $exception = FlattenException::create($e);
                    $handler = new SymfonyExceptionHandler(TRUE);
                    $subject = settings(['solutions_bugnotify', 'bugnotify', 'mail_subject']);
                    \Mail::send('bugnotify_mails::notify', ['css' => $handler->getStylesheet($exception), 'content' => $handler->getContent($exception), 'url' => \Request::fullUrl()], function($message) use ($subject, $emails) {

                        $message->from('support@grapheme.ru', 'BugNotify. Support Grapheme CMS');
                        $message->to($emails)->subject($subject);
                    });
                endif;
            endif;
        endif;
        return $this;
    }

    /**
     * Регистрация в БД критической ошибки
     * @param Exception $e
     * @return $this
     */
    public function logging(Exception $e) {

        if(!in_array($this->statusCode, $this->except)):
            $exception = FlattenException::create($e, $this->statusCode);
            $this->model->insert($exception);
        endif;
        return $this;
    }
}