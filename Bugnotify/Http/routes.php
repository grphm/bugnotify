<?php

\Route::group(['prefix' => 'admin', 'middleware' => 'secure'], function () {
    \Route::get('bugnotify/log', ['as' => 'solutions.bugnotify.log', 'uses' => 'BugnotifyController@viewLaravelLog']);
    \Route::get('bugnotify/log/download', ['as' => 'solutions.bugnotify.log.download', 'uses' => 'BugnotifyController@viewLaravelLogDownload']);
    \Route::resource('bugnotify', 'BugnotifyController',
        [
            'only' => ['index', 'destroy'],
            'names' => [
                'index' => 'solutions.bugnotify.index',
                'destroy' => 'solutions.bugnotify.destroy'
            ]
        ]
    );
});