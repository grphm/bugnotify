<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBugnotifyTables extends Migration {

    public function up() {

        Schema::create('solutions_bugnotify', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 10)->nullable();
            $table->text('description')->nullable();
            $table->text('dump')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down() {

        Schema::dropIfExists('solutions_bugnotify');
    }
}

