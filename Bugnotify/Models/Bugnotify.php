<?php
namespace STALKER_CMS\Solutions\Bugnotify\Models;

use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

/**
 * Модель регистратора ошибок
 * Class Group
 * @package STALKER_CMS\Core\System\Models
 */
class Bugnotify extends BaseModel implements ModelInterface {

    use ModelTrait;
    protected $table = 'solutions_bugnotify';
    protected $fillable = ['code', 'description', 'dump'];
    protected $hidden = [];
    protected $guarded = [];
    protected $casts = ['description' => 'array'];
    protected $dates = ['created_at', 'updated_at'];

    /**
     * @param $exception
     * @return $this
     */
    public function insert($exception) {

        $this->code = $exception->getStatusCode();
        $this->description = [
            'line' => $exception->getLine(),
            'file' => $exception->getFile(),
            'message' => $exception->getMessage(),
            'url' => \URL::full(),
            'method' => \Request::method()
        ];
        $this->dump = json_encode(\Request::all());
        $this->save();
        return $this;
    }

    public function replace($id, $request) {
        // TODO: Implement replace() method.
    }

    /**
     * @param array|int $id
     * @return mixed
     */
    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    /**
     * @param array $attributes
     */
    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    /**
     * @param array $attributes
     */
    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    /**
     * @param array $attributes
     */
    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }
}