<?php

namespace STALKER_CMS\Solutions\Bugnotify\Providers;

use STALKER_CMS\Solutions\Bugnotify\Http\Controllers\BugnotifyController;
use STALKER_CMS\Vendor\Providers\ServiceProvider as BaseServiceProvider;

class ModuleServiceProvider extends BaseServiceProvider {

    public function boot() {

        $this->setPath(__DIR__ . '/../');
        $this->registerViews('solutions_bugnotify_views');
        $this->registerLocalization('solutions_bugnotify_lang');
        $this->registerMailsViews('bugnotify_mails');
        $this->registerConfig('solutions_bugnotify::config', 'Config/bugnotify.php');
        $this->registerSettings('solutions_bugnotify::settings', 'Config/settings.php');
        $this->registerActions('solutions_bugnotify::actions', 'Config/actions.php');
        $this->registerSystemMenu('solutions_bugnotify::menu', 'Config/menu.php');
    }

    public function register() {

        \App::bind('BugnotifyController', function () {
            return new BugnotifyController();
        });

    }

    /********************************************************************************************************************/
    protected function registerMailsViews($namespace) {

        $this->loadViewsFrom($this->RealPath . '/Resources/Mails', $namespace);
    }
}