<?php

return [
    'list' => 'List of errors',
    'logfile' => 'View laravel.log',
    'delete' => [
        'question' => 'Delete entry error number',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete',
    ],
    'empty' => 'List is empty',
];