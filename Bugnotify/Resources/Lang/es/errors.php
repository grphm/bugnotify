<?php

return [
    'list' => 'Lista de errores',
    'logfile' => 'Ver laravel.log',
    'delete' => [
        'question' => 'Entre delet error #',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar',
    ],
    'empty' => 'Lista está vacía'
];