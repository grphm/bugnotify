<?php

return [
    'list' => 'Список ошибок',
    'logfile' => 'Просмотр laravel.log',
    'delete' => [
        'question' => 'Удалить запись об ошибке №',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить',
    ],
    'empty' => 'Список пустой',
];