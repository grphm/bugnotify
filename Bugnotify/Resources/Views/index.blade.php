@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="active">
            <i class="{{ config('solutions_bugnotify::menu.icon') }}"></i> {!! array_translate(config('solutions_bugnotify::menu.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('solutions_bugnotify::menu.icon') }}"></i>
            {!! array_translate(config('solutions_bugnotify::menu.title')) !!}
        </h2>
    </div>
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">@lang('solutions_bugnotify_lang::errors.list')</div>
                <ul class="actions">
                    <li class="dropdown">
                        <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                            <i class="zmdi zmdi-more-vert"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="{!! route('solutions.bugnotify.log') !!}">
                                    @lang('solutions_bugnotify_lang::errors.logfile')
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="card-body card-padding m-h-250 p-0">
                @forelse($errors as $error)
                    <div class="list-group-item media">
                        <div class="pull-right">
                            <div class="actions dropdown">
                                <a href="" data-toggle="dropdown" aria-expanded="true">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a class="c-red js-item-remove" href="">
                                            @lang('solutions_bugnotify_lang::errors.delete.submit')
                                        </a>
                                        {!! Form::open(['route' => ['solutions.bugnotify.destroy', $error->id], 'method' => 'DELETE', 'class' => 'hidden']) !!}
                                        <button type="submit"
                                                data-question="@lang('solutions_bugnotify_lang::errors.delete.question') &laquo;{{ $error->id }}&raquo;?"
                                                data-confirmbuttontext="@lang('solutions_bugnotify_lang::errors.delete.confirmbuttontext')"
                                                data-cancelbuttontext="@lang('solutions_bugnotify_lang::errors.delete.cancelbuttontext')">
                                        </button>
                                        {!! Form::close() !!}
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="lgi-heading">
                                #{!! $error->id !!}. {{ $error->description['message'] }}
                            </div>
                            <small class="lgi-text">{{ $error->description['file'] }}</small>
                            <ul class="lgi-attrs">
                                <li class="c-red">Error code: {!! $error->code !!}</li>
                                <li class="c-purple">Date: {{ $error->created_at->format('d.m.Y H:i') }}</li>
                                <li>Line: {{ $error->description['line'] }}</li>
                                <li>URL: {{ $error->description['url'] }}</li>
                                <li>Method: {{ $error->description['method'] }}</li>
                            </ul>
                        </div>
                    </div>
                @empty
                    <h2 class="f-16 c-gray m-l-30">@lang('solutions_bugnotify_lang::errors.empty')</h2>
                @endforelse
                <div class="lg-pagination p-10">
                    {!! $errors->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop