@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="active">
            <i class="{{ config('solutions_bugnotify::menu.icon') }}"></i> {!! array_translate(config('solutions_bugnotify::menu.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('solutions_bugnotify::menu.icon') }}"></i> {!! array_translate(config('solutions_bugnotify::menu.title')) !!}
        </h2>
    </div>
    <div class="card">
        <div class="list-group">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">Laravel log file</div>
                <small class="lgi-text m-t-5">Path: {!! realpath(storage_path('logs/laravel.log')) !!}</small>
                <small class="lgi-text m-t-5">View the last 1000 lines</small>
            </div>
            <div class="card-body card-padding m-h-250 p-0">
                @if(\File::exists(storage_path('logs/laravel.log')))
                    @set($log, '')
                    @foreach(file(storage_path('logs/laravel.log')) as $index => $line)
                        <?php $log .= $line?>
                        @if($index >= 1000)
                            @break
                        @endif
                    @endforeach
                    <div id="template_content">{!! $log !!}</div>
                @endif
            </div>
        </div>
    </div>
    @if(\File::exists(storage_path('logs/laravel.log')))
        <div class="card">
            <div class="list-group">
                <div class="card-body card-padding">
                    <a class="btn btn-primary btn-icon-text waves-effect"
                       href="{!! route('solutions.bugnotify.log.download') !!}">
                        Download full log File ({!! round(\File::size(storage_path('logs/laravel.log'))/(1024)) !!}
                        Kb)
                    </a>
                </div>
            </div>
        </div>
    @endif
@stop
@section('scripts_after')
    {!! Html::script('core/ace/ace.js') !!}
    <script>
        var editor = ace.edit("template_content");
        editor.setTheme("ace/theme/monokai");
        editor.getSession().setMode("ace/mode/javascript");
        editor.getSession().setUseSoftTabs(true);
        document.getElementById('template_content').style.fontSize = '14px';
        editor.getSession().setUseWrapMode(true);
        editor.setShowPrintMargin(false);
        editor.setOptions({
            maxLines: Infinity
        });
    </script>
@stop